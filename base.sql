CREATE TABLE employees(
id int(10) unsigned NOT NULL AUTO_INCREMENT,
name char(20) COLLATE utf8_general_ci NOT NULL,
surname char(20) COLLATE utf8_general_ci NOT NULL,
gender enum('male', 'female'),
phone char(12) COLLATE utf8_general_ci DEFAULT NULL,
birthday data  NOT NULL,
education int(15) COLLATE utf8_general_ci NOT NULL,
salary char (5) COLLATE utf8_general_ci DEFAULT NULL
PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci; 

show table




CREATE TABLE employees(
id int(10) unsigned NOT NULL AUTO_INCREMENT,
name char(20) COLLATE utf8_general_ci NOT NULL,
surname char(20) COLLATE utf8_general_ci NOT NULL,
gender enum('male', 'female'),
phone char(12) COLLATE utf8_general_ci DEFAULT NULL,
birthday data ,
education char (255) ,
salary number ( 10 , 2) DEFAULT NULL
PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci; 
