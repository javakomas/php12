PHP-12 MySQL Select sakiniai, PHP-PDO
SELECT

SQL sakinys SELECT yra pagrindinis sakinys DB-je esantiems duomenims peržiūrėti. Šis sakinys turi daug įvairiausių variantų ir galimybių. Išsamiai šio sakinio sintaksei pateikti prireiktų keleto puslapių. Sakinį bendriausiu atveju galima užrašyti taip:

SELECT [DISTINCT] <stulpelių vardai>
FROM <lentelių vardai>
[WHERE <paieškos sąlyga>]
[GROUP BY <stulpelių vardai> [HAVING <Paieškos sąlyga>]]
[ORDER BY <stulpelių vardai>] .

Įvykdžius šį sakinį, DBVS suformuoja ir pateikia vartotojui užklausos rezultatą – laikiną lentelę, kuri egzistuoja tik užklausos rezultato peržiūros metu. Sistemos vartotojas rezultatą gali pamatyti monitoriaus ekrane ar apdoroti jį programoje.

Nesunku pastebėti, kad pati paprasčiausia užklausa atrodo taip:

SELECT * FROM <lentelės vardas>;

SELECT užklausą sudaro kelios dalys.

Pirmoji dalis, iš karto po žodžio SELECT, yra laukų sąrašas. Pavyzdžiui:

SELECT commission, employee_number FROM sales_rep WHERE surname='Gordimer';


SELECT *

Taip pat galima panaudoti ir pakaitos simbolį (*), kad būtų pateikti visi

laukai, kaip čia:

SELECT * FROM sales_rep WHERE surname='Gordimer';

Pakaitos simbolis * atitinka visus lentelės laukus. Taigi prieš tai pateiktame pavyzdyje pateikiami visi keturi laukai tokia pačia tvarka, kokia jie išrikiuoti lentelės struktūroje.

Select * pagal gerąsias praktikas negalima naudoti PHP kode, todėl, kad db struktūra pastoviai keičiasi ir prisideda nauji stulpeliai ir keičiasi stulpelių pozicijos, dėl to ateity iškyla problemų palaikant tokį kodą

Visad naudoti select(fieldName, fieldName2,..) FROM table1...
DISTINCT

Vienodų eilučių galima išvengti panaudojant bazinį žodį DISTINCT. Sakinį perrašę taip:

SELECT DISTINCT Išsilavinimas FROM Vykdytojai

Įtraukus DISTINCT sakinį vienodos eilutės pašalinamos. Neįtraukus į užklausą bazinio žodžio DISTINCT, užklausos rezultate galimos vienodos eilutės, tiksliau, vienodos eilutės nėra pašalinamos.

AS

Kai kurie stulpeliai gali būti apskaičiuojami. Tarkime, kad lentelėje Projektai projektų trukmė nurodyta mėnesiais, o mes tą laiką norime sužinoti dienomis. Paprastumo dėlei tarkime, kad kiekviename mėnesyje yra 30 dienų. Uždavinį išspręsime sakiniu:

SELECT Pavadinimas, Trukmė * 30 FROM Projektai .

Šiame sakinyje yra pavartotas skaičius 30 – tai skaitinių duomenų konstanta. Skaičiai SQL sakiniuose rašomi be jokių papildomų atskyrėjų. Šios užklausos rezultatas yra laikina lentelė, turinti du stulpelius.

Kai užklausos SELECT frazėje vartojamas reiškinys, rezultato stulpelio pavadinimas gali būti dviprasmiškas.

Tokiais atvejais sistema, vaizduodama užklausos rezultatą, stulpeliui suteikia tarnybinį pavadinimą, kuris atitinka stulpelio eilės numerį užklausoje (antrajam stulpeliui suteikiamas vardas “2”). Tai nevisada priimtina.

Vartotojas gali pats nurodyti norimą stulpelio pavadinimą, pavartojant bazinį žodį AS.

Suteikiamas stulpeliui pavadinimas turi tenkinti tuos pačius reikalavimus, kaip ir duomenų bazės stulpelio pavadinimas.

Norint stulpeliui suteikti pavadinimą, kuriame būtų vienas ar keli specialieji simboliai, būtina stulpelio pavadinimą rašyti tarp kabučių:

SELECT Pavadinimas, Trukmė * 30 AS “Trukmė dienomis” FROM Projektai.


WHERE

SELECT sakinio dalis, esanti po WHERE, yra vadinama sąlygos sakiniu. Šis sakinys labai lankstus ir jame gali būti daugybė įvairių tipų sąlygų. Pažvelkite į šį pavyzdį:

SELECT * FROM sales_rep WHERE commission>10 OR surname='Rive' AND first_name='Sol';


Dabartinė data ir kt. konstantos

Užklausose jau vartojome pačius paprasčiausius reiškinius: konstantas, stulpelių pavadinimus ir aritmetinius reiškinius.

Vardinės konstantos (sisteminiai pseudokintamieji) - tai reikšmės, kurias saugo DBVS ir kurias galima vartoti SQL sakiniuose. Paprastai, tai išorinė DBVS požiūriu informacija, žyminti einamąją sisteminę datą

(CURRENT_DATE), laiką (CURRENT_TIME), datą ir tikslų laiką (CURRENT_TIMESTAMP), sistemos vartotojo vardą (USER) ir pan. Kai kuriose DBVS, sisteminės reikšmės pasiekiamos ne per vardines konstantas, bet kviečiant specialias funkcijas


Palyginimas, predikatai

    Reiškiniai gali būti predikatų argumentais (operandais).
    Predikatas - tai sąlyga lentelės eilutei ar eilučių grupei, kuri gali būti teisinga, neteisinga arba neapibrėžta.
    SQL leidžiamos tokios palyginimo operacijos: =, <, <=, >, >=, <>.
    Jei palyginimo operacijose vienas iš operandų yra NULL, tai predikato rezultatas yra neapibrėžtas. Palyginimo operacijose operandais gali būti ne tik skaitiniai duomenys, bet ir tekstiniai bei datos ir laiko duomenys.

Prie paprasčiausių predikatų priskiriama:

◦ x BETWEEN y AND z - rezultatas teisingas tik tuomet, kai x reikšmė yra tarp y ir z, t.y. kai x >= y ir x <= z;

◦ x NOT BETWEEN y AND z - rezultatas teisingas tik tuomet, kai x nėra tarp y ir z, t.y. kai x < y arba x > z;

◦ x IN (y1, y2,..., yn) - rezultatas teisingas tik tuomet, kai x reikšmė sutampa bent su viena iš reikšmių y1, y2,..., yn;

◦ x NOT IN (y1, y2,..., yn) - rezultatas teisingas tik tuomet, kai x nesutampa nei su viena iš reikšmių y1, y2, ..., yn;

◦ x IS NULL - rezultatas yra teisingas tik tuomet, kai reiškinio x reikšmė yra NULL;

◦ x IS NOT NULL - rezultatas yra teisingas tik tuomet, kai reiškinio x reikšmė nėra NULL.


Like

    x LIKE y - rezultatas teisingas tik tuomet, kai simbolių eilutė x yra “panaši” į simbolių eilutę y.
    x NOT LIKE y - rezultatas yra teisingas tik tuomet, kai simbolių eilutė x nėra panaši į simbolių eilutę y.

Eilutėje y gali būti pavartoti tie patys simboliai kaip ir predikate LIKE;

Jei tarkime pamenate, kad pavardė prasideda Petr, galite įvykdyti tokią užklausą:

SELECT * FROM darbuotojai WHERE pavarde LIKE ' Petr % ';


Šablono atitikimas: LIKE ir %

% - tai pakaitos simbolis, panašiai kaip ir simbolis *, tačiau skirtas naudoti tik SELECT sąlygose. Jis reiškia 0 arba daugiau simbolių.

Pakaitos simbolis dažnai dedamas į frazės pradžią ar pabaigą pvz.i:

SELECT * FROM pardavimai WHERE pavarde LIKE '%e%'


Pavyzdžiai


Pavyzdžiui, projektų, kurių pavadinime yra frazė “apskaita”, jų svarba yra vidutinė ar didelė, ir kurie turėjo būti pabaigti iki šiandien, pavadinimus, vykdymo pradžios bei pabaigos datas galima sužinoti tokia užklausa:

SELECT Pavadinimas, Pradžia, Pradžia+Trukmė MONTHS AS Pabaiga
FROM Projektai
WHERE Pavadinimas LIKE ‘%apskaita%’ AND
Svarba IN ('Vidutinė', 'Didelė')

Informacija apie vykdytojus – informatikus arba vykdytojus, baigusius Vilniaus universitetą (nepriklausomai nuo kvalifikacijos) ir turinčius aukštesnę nei trečią kategoriją, bus pateikta įvykdžius užklausą:

SELECT * FROM Vykdytojai 
WHERE Kvalifikacija = 'Informatikas'
OR (Išsilavinimas = 'VU' AND Kategorija > 3).


ORDER BY

Kitas naudingas ir dažnai naudojamas sakinys leidžia rikiuoti rezultatus

Būtų naudinga pamatyti abėcėles tvarka išrikiuotą darbuotojų sąrašą, ir norėdami ji išvysti, galite panaudoti ORDER BY sakinį:

SELECT * FROM pardavimai ORDER BY pavarde;

Šis sąrašas nebus visiškai teisingas, jeigu norite išrikiuoti pagal vardus,

kadangi Pavardės gali sutapti, o vardai skirtis. Norėdami tai pataisyti,

turite išrikiuoti ir pagal vardą tuo atveju, jei pavardės sutampa.

Kad tai padarytumėte, turite įvykdyti tokią užklausą:

SELECT * FROM pardavimai ORDER BY pavarde, vardas;


Norėdami išrikiuoti atvirkščia tvarka (mažėjančia), turite panaudoti reikšmini žodi DESC. Kita užklausa grąžins visus įrašus, išrikiuotus pagal uždirbtus komisinius, pradedant didžiausiu ir baigiant mažiausiu:

SELECT * FROM pardavimai ORDER BY komisiniai DESC;


LIMIT

Realioje duomenų bazėje gali būti ne vienas tūkstantis įrašų ir jūs vienu metu nenorėsite pamatyti jų visų.

Todėl MySQL leidžia panaudoti LIMIT sąlygą.

LIMIT nėra standartiniame SQL, o tai reiškia, kad negalėsite jo taip pat panaudoti ir visose kitose duomenų bazėse, tačiau tai naudingas ir galingas MySQL įrankis.

SELECT vardas, pavarde, komisiniai FROM pardavimai ORDER BY komisiniai DESC LIMIT 1:

LIMIT sakinys leidžia ne tik išvesti ribotą skaičių jrašų, skaičiuojant nuo pradžios ar pabaigos.


OFFSET

Jūs galite nurodyti MySQL, nuo kelintos eilutės skaityti įrašus ir kiek eilučių rodyti. Jeigu LIMIT sąlygojė yra du skaičiai, pirmasis žymi praleidžiamų, o antrasis - rodomų eilučių skaičių.

Tolesniame pavyzdyje išvedamas antrasis įrašas, kuomet duomenys išrikiuoti mažėjančia tvarka:

SELECT vardas, pavarde, komisiniai FROM pardavimai ORDER BY komisiniai DESC LIMIT 1,1

arba

SELECT vardas, pavarde, komisiniai FROM pardavimai ORDER BY komisiniai DESC LIMIT 1 OFFSET 1


Dažnai tokios užklausos naudojamos puslapiavimui, pvz. LIMIT 50 OFFSET 150 - 4 puslapis, viename psl po 50 įrašų


Funkcijos

    Funkcija - tai operacija, nusakoma funkcijos vardu ir apskliaustais lenktiniais skliaustais argumentais, kurie tarpusavyje atskiriami kableliais (atskiru atveju argumentų gali ir nebūti).
    Funkcija visuomet grąžina tam tikrą rezultatą (tai gali būti ir specialioji reikšmė NULL).
    Funkcijos yra skirtomos į agregatines (stulpelių) ir skaliarines.

    Skaliarinės funkcijos argumentas visuomet yra viena reikšmė. Funkcija gali turėti ir kelis argumentus, tačiau kiekvienas argumentas – viena reikšmė, o ne reikšmių aibė, kaip agregatinėje funkcijoje.
    Vartojant skaliarines funkcijas frazėje WHERE, funkcijos rezultatas apskaičiuojamas tikrinant paieškos sąlygą kiekvienai lentelės eilutei atskirai.
    Šios rūšies funkcijos dažnai vartojamos ir SELECT frazėje, kuometrezultatas skaičiuojamas ne visoms eilutėms iš karto, kaip yra agregatinių funkcijų atveju, o kiekvienai eilutei atskirai.


DBVS leidžia vartoti gana daug skaliarinių funkcijų. Paminėsime tik
keletą iš jų:
◦ DAY(<data>) – diena (skaičius nuo 1 iki 31) datoje,
◦ MONTH(<data>) – mėnesis datoje,
◦ YEAR(<data>) – metai datoje,
◦ LENGTH(<simbolių eilutė>) – simbolių eilutės ilgis,
◦ SUBSTR(<simbolių eilutė>, <pradžia>, <ilgis> ) – simbolių
eilutės fragmentas

ir pan.

tekstų sujungimui naudoti CONCAT(<eil1>, <eil2>, <eil3>,...);
Operacijos

Iš konstantų, kreipinių į funkcijas bei vardų, žyminčių DB objektus,
panaudojant operacijas, galima sudaryti paprasčiausius reiškinius.
SQL leidžia naudoti šias operacijas:

+ (sudėtis),
- (atimtis),
* (daugyba),
/(dalyba),

Jei bent vienas iš pateiktų operacijų argumentų yra NULL, tai ir
rezultatas yra NULL.


PHP Jungimasis prie DB

https://phpdelusions.net/pdo

https://phptherightway.com/#databases


Yra 3 mysql bibliotekos

    mysql - sena biblioteka, nerekomenduojama naudoti, neturi "parameter binding", pavojinga dėl sql injection
    mysqli - atnaujinta mysql biblioteka
    PDO - rekomenduojama naudoti, objektinė išraiška prisijungui, vietoj funkcijų 


PDO

Inicializacija, naudojant jūsų konfigūraciją:

$db = new PDO('mysql:host=localhost;dbname=testdb;charset=utf8mb4', 'username', 'password');

Blogas pavyzdys

$pdo->query("SELECT name FROM users WHERE id = " . $_GET['id']); // <-- NO!

Geras pavyzdys, parametrų susiejimas naudojant ":param" :

$id = 1; // or  (int) $_GET['id']
$stmt = $pdo->prepare('SELECT name FROM employees where id= :id');
$stmt->execute(['id' => $id]);
$result = $stmt->fetch();
echo 'Name is ' . $result['name'];


FetchAll. Atvaizdavimas gautos informacijos:

$stmt = $pdo->prepare('SELECT name FROM employees');
$stmt->execute();
$result = $stmt->fetchAll();
echo '<ul>';
foreach ($result as $result) {
    echo '<li>' . $result['name'] . '</li>';
}
echo '</ul>';